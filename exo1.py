#!/usr/bin/env python3

import logging
import string
import argparse
import sys


def display_i(file):
    """open file and display line with letter 'i' """
    logging.debug("Lignes contenant la lettre 'i'")
    logging.info('-----------')
    with open(file, 'r') as f:
        list(map(lambda x: logging.info(x.strip("\n")),
             list(filter(lambda line: 'i' in line, f))))
    logging.info('-----------')


def not_null_alpha(file):
    """open file and display non-blank line"""
    logging.debug("Triées (Ordre inverse : False)")
    logging.info('-----------')
    with open(file, 'r') as f:
        list(map(lambda x: logging.info(x.strip("\n")),
             list(sorted(filter(lambda line: line != '\n', f)))))
    logging.info('-----------')


def not_null_alpha_rev(file):
    """open file and display non-blank line sorted"""
    logging.debug("Triées (Ordre inverse : True)")
    logging.info('-----------')
    with open(file, 'r') as f:
        list(map(lambda x: logging.info(x.strip("\n")),
             list(sorted(filter(lambda line: line != '\n', f), reverse=True))))
    logging.info('-----------')


def set_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('path', metavar='file', nargs='+',
                        help='Fichiers à traiter')
    parser.add_argument('-v', '--verbose',
                        action="store_true", help='Mode verbeux')
    return parser.parse_args()


def _skip():
    skip = input("Passer y/[n]?")
    if skip in {"y", "Y"}:
        pass
    else:
        logging.info('file not found')
        logging.info('Fin du prog')
        sys.exit(2) #ENOENT


def handle_file(file):
    try:
        logging.debug(f"traitement du fichier {file}")
        display_i(file)
        not_null_alpha(file)
        not_null_alpha_rev(file)
        logging.debug(f"traitement du fichier {file}")
    except FileNotFoundError:
        _skip()


if __name__ == '__main__':
    format_string = ('%(asctime)s [%(filename)s:%(funcName)s#%(process)d]'
                     '%(levelname)s: %(message)s')

    logging.basicConfig(
        format=format_string, datefmt="%Y-%m-%d %H:%M:%S", level=logging.INFO)

    args = set_parse()
    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    list(map(handle_file, args.path))
